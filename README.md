# Test task for Front End Developer – React with NextJS
## Kostrov Pavel
### Contacts: 
- pashakostrov.pk@gmail.com
- 380997021613

# Description:
- Create a mini Web App that displays the current token price. 
- You should use ReactJS with NextJS and a redux state. To fetch the price of BTC you can use the Coindesk API (https://api.coindesk.com/v1/bpi/currentprice.json).
- The price should be stored in the Redux state and regularly updated. 
- Focus on good project structure and information flow.

### Goals:
- [x] Create new NextJS app
- [x] Use ReactJS
- [x] Use Redux state
- [x] Fetch BTC price 
- [x] Display BTC price on a page


### Bonus points:
- [ ] Use Docker for deployment
- [ ] Display the price of multiple tokens
- [x] Use .scss for styling
