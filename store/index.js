import { updateReducer } from "./reducers/changeReduce.js";
import { configureStore } from "@reduxjs/toolkit";

export const store = configureStore({ reducer: updateReducer });
