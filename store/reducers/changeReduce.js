const defaultState = {};

export const updateReducer = (state = defaultState, action) => {
  const { payload } = action;
  switch (action.type) {
    case "set_data":
      return {
        ...state,
        time: payload.time,
        disclaimer: payload.disclaimer,
        bpi: payload.bpi,
        chartName: payload.chartName,
      };
    case "update_data":
      return { ...state, time: payload.time, bpi: payload.bpi };
    default:
      return state;
  }
};
