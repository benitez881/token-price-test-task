import styles from "./Footer.module.scss";

function Footer() {
  return (
    <div className={styles.footer__container}>
      <div>Contacts:</div>
      <div>E-mail: pashakostrov.pk@gmail.com</div>
      <div>
        <span>Github: </span>
        <a href="https://github.com/benitez881" className={styles.footer__link}>
          benitez881
        </a>
      </div>
    </div>
  );
}

export default Footer;
