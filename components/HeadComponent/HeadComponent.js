import Head from "next/head.js";

function HeadComponent() {
  return (
    <Head>
      <title>Bitcoin price</title>
      <meta name="description" content="Bitcoin price test task" />
      <link rel="icon" href="/favicon.ico" />
      <meta httpEquiv="Content-Type" content="text/html; charset=utf-8"></meta>
    </Head>
  );
}

export default HeadComponent;
