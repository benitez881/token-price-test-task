import Footer from "../Footer/Footer.js";
import HeadComponent from "../HeadComponent/HeadComponent.js";

function Layout({ children }) {
  return (
    <div className="wrapper">
      <HeadComponent></HeadComponent>
      {children}
      <Footer></Footer>
    </div>
  );
}

export default Layout;
