import styles from "./Card.module.scss";

const Card = ({ data }) => {
  const { chartName, disclaimer, bpi, time } = data;
  const bpiArr = Array.from(Object.entries(bpi));
  return (
    <div className={styles.card} description={disclaimer}>
      <h2>{chartName}</h2>
      <h3>Updated at: {time.updated}</h3>
      <h4>Price:</h4>
      <ul>
        {bpiArr.map((value, index) => {
          return (
            <li className={styles.card__price_listitem} key={index}>
              {value[1].code} - {value[1].rate}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Card;
