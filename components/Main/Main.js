import { useDispatch, useSelector } from "react-redux";
import Card from "../Card/Card";
import useInterval from "../../hooks/useInterval.js";
import styles from "./Main.module.scss";
import getCurrentPrice from "../../api/currentPrice";

function Main({ data }) {
  const dispatch = useDispatch();
  const {
    time: timeState,
    rate: rateState,
    bpi: bpiState,
    disclaimer: disclaimerState,
  } = useSelector((state) => state);
  if (!timeState && !rateState && !bpiState && !disclaimerState) {
    const { time, chartName, bpi, disclaimer } = data;
    dispatch({
      type: "set_data",
      payload: {
        time,
        disclaimer,
        bpi,
        chartName,
      },
    });
  }
  const stateData = useSelector((state) => state);

  useInterval(() => {
    (async function () {
      const data = await getCurrentPrice();
      const { time, bpi } = data;
      dispatch({
        type: "update_data",
        payload: {
          time,
          bpi,
        },
      });
    })();
  }, 30000);

  return (
    <div className={styles.container}>
      <h1 className={styles.header}>Bitcoin price</h1>
      <Card data={stateData}></Card>
    </div>
  );
}

export default Main;
