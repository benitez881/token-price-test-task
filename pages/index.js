import Main from "../components/Main/Main.js";
import Layout from "../components/layout/Layout.js";
import getCurrentPrice from "../api/currentPrice.js";

export default function Home({ data }) {
  return (
    <Layout>
      <Main data={data}></Main>
    </Layout>
  );
}

export async function getServerSideProps() {
  const data = await getCurrentPrice();
  return {
    props: { data }, // will be passed to the page component as props
  };
}
